## Τρέχουσα διαδικασία development από design signoff και μετά

1. Παίρνει τα σχέδια
- **IA:** Ξεκινάει η διαδικασία της ανάλυσης των σχεδίων σε επίπεδο αρχιτεκτονικής πληροφορικής και επακολούθως οργανώνεται και η δομή του Drupal (Content types, paragraph types, taxonomy terms, views, blocks types, forms, view modes).
- **VISUAL:** Ξεκινάει τη διαδικασία της ανάλυσης των σχεδίων σε επίπεδο οπτικό Componentization (layouts, components, variations, colors).

2. Υλοποίηση
  - Υποχρεωτικά λόγω Drupal, παρόντων εργαλείων και προσέγγισης θα πρέπει αρχικά να φτιάξεις όλο το site building ή αυτά που χρειάζεσαι για τη σελίδα.
  - Θα πρέπει να το γεμίσεις με dummy data για να το δεις (είτε σε template ή drupal content επίπεδο).

## Cons
- Back and forth από structure/drupal/site building σε twig/sass κτλ.
- Δύσκολη συγκέντρωση και απομόνωσσ/τμηματοποίηση εργασίας.
- Design process & frontend είναι αποκομμένα.
  - Hellasfin: Could not work independently without Stelios' work.
- Frontend & backend δεν είναι διαχωρισμένα.
- Τα σχέδια γίνονται export μόνο ανα σελίδα.
- Missing Design tokens (color palette, Typography (text, titles) etc.).
- Κάθετο workflow (site building σελίδας -> layouts, components σελίδας -> σύνθεση σελίδας).
- Internet explorer support costs more -> Charge more? (css grids, css variables).
- A lot of components to start as a boilerplate. It might be better to add them as assets. Something like https://bit.dev/
- Existing components too complex structure and hard to reuse and find.
- Too much drupal classes with 'classy' theme.

## Recommendations
### Dev
- Use storybookjs.
  - https://storybook.js.org/
  - https://gitlab.com/arismag/storybook
  - Open source with strong community and fast evolving.
  - Slack/discord channels for direct support.
  - **Greek community:** bserem, george (zehnplus) will probably start to use it and Zekvyrim (amazee) already uses it as a backend developers.
  - **amazee** uses storybook and GraphQL [repository](https://github.com/AmazeeLabs/silverback). One of the major supporters for drupal.
  - **Fourkitchen** uses storybook (with emulsify theme) [repository](https://github.com/fourkitchens/emulsify). One of the other major supporters for drupal :P.
  - UI inventory.
    - [Example](https://ux.mailchimp.com/patterns/color)
    - [Interface inventory](https://bradfrost.com/blog/post/interface-inventory/)
  - Common language between designer/developers/clients.
  - Instant development.
  - [Faker](https://github.com/marak/Faker.js/) for random data.
  - No drupal or any CMS/Framework/API/.. dependency and loading.
  - Instant and fast **styling**.
  - Instant and fast **testing**.
    - Visual testing.
  - Simpler maintenance.
  - Shorter learning curves
  - QA takes place before any integration and not only on final stage.
  - Creating components first in this environment makes sure we catch analysis error before site building. (less effort on site building changes).
- Maybe use GraphQL.
  - Decouple drupal with direct database queries.
  - Backend/Frontend even more fragmented.
- Maybe use [webpack bundler/drupal](https://thinkshout.com/blog/2019/10/adding-webpack-to-a-traditional-drupal-theme/).
- Standarize components and drupal integration. Component twig should be different from
- Use prefixes for all patterns. (https://www.bvs.ch/kader) | Benefits: Immediate classes recognition, and where to find each component. (https://bradfrost.com/blog/post/css-architecture-for-design-systems/)
    - ![alt](images/prefix.png)
- Design token creation from design export directly. Check design chapter below.

![alt](images/tokens.png)

- [Design token addon](https://github.com/UX-and-I/storybook-design-token)
- [Design token addon demo](https://storybook-design-token.netlify.com/?path=/story/*)
- JS should be on component/pattern directory. If we want it to have on drupal js theme base, create symlinks with a script.
- Global and per component scss.
  - Use `__` or `_b_` `_b.pattern.scss` prefix for base components scss. Compile all to one css file.
  - Use `_` prefix for single components. Compile to a single file and attach it per component.
  - [Libraries and drupal](https://www.previousnext.com.au/blog/performance-improvements-drupal-8-libraries)
- [Drupal integration and libraries](https://medium.com/@askibinski/integrating-storybook-with-drupal-ddabfc6c2f9d)
- Create script for pattern creation (input: pattern_name, output: directory, pattern_name.twig with basic markup, pattern_name.stories.ts with standard defintion, pattern_name.md)
- Pattern creation decision diagram https://coggle.it/diagram/V0hkiP976OIbGpy8/t/vanilla-pattern
- You could even create components (markup and data only) from the wireframes or IA.

### Design
- Design/Organize sketch files per components (each component a symbols/library sets) and export on sketch. [example](https://medium.com/sketch-app-sources/design-systems-sketch-how-to-start-preparing-ui-components-library-e4f827315646)
- Create design tokens in sketch and export them in storybook.
  - [Design token sketch exporter](https://sketchpacks.com/here-erhe/Design-Token-Exporter)
  - [Design tokens, what and howto](https://medium.com/eightshapes-llc/tokens-in-design-systems-25dd82d58421)
  - [Design token extras](https://uxdesign.cc/building-a-design-system-where-to-start-part-5-bonus-elements-2b4bdd614b9f)
  - [Design tokens extras](https://material.io/design/layout/responsive-layout-grid.html#grid-customization)
- Commit design in repository. Each project could have different repositories.
- Commit html/css sketch files and use a git submodule.
  - One for Design.
  - One for code.
  - One for storybook.
- Publish exported design in html/css via CI/CD on a server and link each component storybook to relative component. (https://medium.com/@brandaiapp/connect-sketch-symbols-with-their-react-counterparts-d100be414acd)
- Storybook to sketch (https://github.com/html-sketchapp/html-sketchapp)
### Project management
1. Issue per component/pattern.
  2. Each component is related to an issue.
    - Each component have 5 states:
      - `indesign` (probably not needed)
      - `inprogress`
      - `inreview`
      - `inintegration`
      - `complete`

### Pattern Workflow
![alt](images/storybookWorkflow.png)

1. AI
2. Wireframe/Design
3. Implementation
4. QA
  - with Design on visual.
  - QA with IA
5. Changes. GOTO 2.
6. Release


- [Reference](https://blog.hichroma.com/the-delightful-storybook-workflow-b322b76fd07)

## Pattern structure
### 00 - Design Tokens/Tokens/Visual Style/Styleguide
- #### 00 - Colors
    - Colors
    - All availables.
    - Primaries
    - Secondaries
    - Tertiary
    - States
    - Functional (if needed)
        - Background Color
        - Text Color
        - Border Color

- #### 01 - Typography
    - Title style/text
        - Font/Typeface
        - Font Size
        - Letter spacing
        - Opacity
        - Line Height

- #### 02 - Spacing
    - Margins
    - Columns
    - Gutters

- #### 03 - Breakpoints
- #### 04 - Icons
- #### 05 - Time and animation
- #### 06 - Shadows


### 01 - Base
All base elements.

### 02 - Layouts
Grids, etc.

### 03 - Components
- Common (Resusable/usefull/common)
- Per project (Usable)

#### Shared pattern library
- Inventory of usefull components.
- Download and adapt the ones you need for a project.
- Upload only the ones you consider usefull for other projects as well.

> Either way, don’t expect your component to be reused out-of-the-box. Built it as a reusable template that can be modified for future usage. With the right tools, that makes all the difference. Many components meet this criterion.

1. [BIT](https://bit.dev/components)
2. [reusable components 1](https://blog.bitsrc.io/do-we-really-use-reusable-components-959a252a0a98)
3. [reusable components 2](https://blog.bitsrc.io/shared-components-best-practices-for-2019-c259cdc3b623)


- NPM module per component. (too much work)
- GIT submodules. (tricky git syncing)
- Central PB Storybook
  - Host on a storybook.
  - Patterns as assets, each one has a download link.
  - Monolithic repo or multi-repo?
  - Every pattern folder.
  - Patterns dependencies
    - SASS VARIABLES
    - MIXINS/EXTENDS
    - POSSIBLE SOLUTIONS:
      - SASS only as source code.
      - Compiled css for each component/pattern.
      - Auto document dependencies and maybe link conflicted patterns.
      - Each component gets uploaded with its design token dependencies. Or standarize design tokens (primary, secondary, etc)
    - CI/CD automation needed..

### 04 - Pages

## PROBLEM
- Gap between IA/DESIGN/FRONTEND.
- Η φάση της αρχιτεκτονικής πληροφορικής με το wireframing/design πρέπει να εμπλέκει τόσο των υπεύθυνο του πρότζεκτ και τον γραφίστα αλλά και τους developers. Η διαδικασία του component driven design μπορεί ξεκινάει πρίν τα σχέδια.
- Δεν υπάρχει λόγος να γίνεται διπλή δουλειά.
- Είναι διαδικασίες παράλληλες.

![alt](images/jjgarret.png)
![alt](images/jjgarret2.jpg)

- Share designs and get feedback on cloud.
  - https://www.invisionapp.com/sketch-prototyping
  - https://www.invisionapp.com/plans
  - https://uxtools.co/tools/prototyping/
  - Other tools [Comparison](https://blog.prototypr.io/design-collaboration-tools-in-2019-e2b7163bc088)

- Maybe use figma for wireframing.
  * [Figma project](https://www.figma.com/file/5oPuKcxjFzQ5jVD8BtwU99/Wireframes?node-id=5509%3A3733)
  * [Mobile view](https://www.figma.com/proto/5oPuKcxjFzQ5jVD8BtwU99/Wireframes?node-id=5509%3A3733&scaling=scale-down)
  * [Desktop view](https://www.figma.com/proto/5oPuKcxjFzQ5jVD8BtwU99/Wireframes?node-id=5509%3A3734&scaling=min-zoom)

## Worklflow??
00 - Analysis
  - Client meeting
  - Requirements analysis (signoff??)
  - Information Architecture (signoff??)

02 - Design
03 - Development
04 - Quality
04 - Launch
00 - Maintenance and Support

## Other CSS techniques
- [CSS variables](https://www.freecodecamp.org/news/how-to-make-responsiveness-super-simple-with-css-variables-8c90ebf80d7f/)
  - https://caniuse.com/#feat=css-variables
  - https://github.com/MadLittleMods/postcss-css-variables
- CSS grids
  - https://caniuse.com/#search=css%20grids
- [Fluid Typography](https://medium.com/sketch-app-sources/truly-fluid-typography-257a2b434105)
## other
- https://blog.hichroma.com/design-systems-in-storybook-2b2be06e394b
- https://help.brand.ai/hc/en-us/articles/360033417192
- https://github.com/christophersmith262/drupal-libraries-webpack-plugin
